<p align="center">
    <img src="https://github.githubassets.com/images/modules/logos_page/GitHub-Mark.png" width="200" />
</p>
<h1 align="center">Github Issue Tracker Back-End</h1>

<h2>Installing</h2>
<p>Preliminary requirements:</p>
<ul>
    <li><a href="https://nodejs.org/">Node.js</a> >= <code>18.12.1</code></li>
    <li><a href="https://www.npmjs.com/">npm</a> >= <code>9.1.2</code></li>
    <li><a href="https://yarnpkg.com/">yarn</a> >= <code>1.22.19</code></li>
    <li><a href="https://www.mongodb.com/">MongoDB</a> >= <code>7.0.0</code></li>
</ul>
<p>Installing dependencies:</p>
<pre>yarn</pre>

<h2>Starting & Building</h2>
<p>Start application to development mode:</p>
<pre>yarn start</pre>
<p>Build application to production:</p>
<pre>yarn build</pre>

<h2>Prettier</h2>
<p>Format code (with Prettier):</p>
<pre>yarn format</pre>

<h2>Environment variables</h2>
<p>List of variables:</p>
<ul>
    <li><code>PORT</code> - Server port listening. Default: 80.</li>
    <li><code>GITHUB_API_URL</code> - Github Api Url. Example: https://api.github.com/.</li>
    <li><code>GITHUB_AUTH_TOKEN</code> - Github Auth Token.</li>
    <li><code>MONGODB_URI</code> - MongoDB URI.</li>
</ul>
<p><a href="https://www.npmjs.com/package/dotenv">Dotenv</a> support is present!</p>

<h2>License</h2>
<p><a href="./LICENSE">MIT</a></p>
