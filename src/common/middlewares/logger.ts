import {NextFunction, Request, Response} from "express";
import requestIp from "request-ip";
import {Log} from "@common/models/log";

export interface LoggerMiddlewareOptions {
    type: string;
}

export function loggerMiddleware({type}: LoggerMiddlewareOptions) {
    return async (req: Request, res: Response, next: NextFunction) => {
        const log = new Log();

        log.type = type;
        log.userIp = requestIp.getClientIp(req) ?? "0.0.0.0";
        log.time = new Date();

        await log.save();

        return next();
    };
}
