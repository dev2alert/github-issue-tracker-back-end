import {config} from "dotenv";
import {join} from "path";

export const mode: string = process.env.NODE_ENV ?? "production";

export const devMode: boolean = mode === "development";

config();
if (devMode) {
    config({
        path: join(process.cwd(), "./.env.local")
    });
    config({
        path: join(process.cwd(), "./.env." + mode)
    });
}

export const serverPort: number = Number(process.env.PORT ?? 80);

export const githubApiUrl: string = process.env.GITHUB_API_URL ?? "/";

export const githubAuthToken: string | null = process.env.GITHUB_AUTH_TOKEN ?? null;

export const mongodbUri: string = process.env.MONGODB_URI ?? "/";
