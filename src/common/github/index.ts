import axios from "axios";
import {githubApiUrl, githubAuthToken} from "@common/config";

export const githubApi = axios.create({
    baseURL: githubApiUrl,
    headers: {
        Accept: "application/vnd.github+json",
        "X-GitHub-Api-Version": "2022-11-28",
        Authorization: githubAuthToken !== null ? "Bearer " + githubAuthToken : undefined
    }
});
