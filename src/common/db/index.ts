import mongoose from "mongoose";
import {mongodbUri} from "@common/config";

export const connectDb = () => mongoose.connect(mongodbUri);
