import {Schema, model} from "mongoose";

export const logSchema = new Schema({
    type: String,
    time: Date,
    userIp: String
});

export const Log = model("logs", logSchema);
