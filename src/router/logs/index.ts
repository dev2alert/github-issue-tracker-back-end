import {Router} from "express";
import {Log} from "@common/models/log";

export function createLogsRouter(): Router {
    const router = Router();

    router.get("/", async (req, res) => {
        const page = Number(req.query["page"] ?? 1);
        const perPage = Number(req.query["perPage"] ?? 10);

        const items = await Log.find()
            .skip((page - 1) * perPage)
            .sort({time: "desc"})
            .limit(perPage);
        const total = await Log.find().count();

        res.send({
            result: "success",
            items,
            page,
            perPage,
            total,
            totalPages: Math.ceil(total / perPage)
        });
    });

    return router;
}
