import {Router} from "express";
import {AxiosError} from "axios";
import {githubApi} from "@common/github";
import {loggerMiddleware} from "@common/middlewares/logger";

export function createIssuesRouter(): Router {
    const router = Router();

    router.get("/", loggerMiddleware({type: "get_issues"}), async (req, res) => {
        const owner: string = String(req.query["owner"] ?? "facebook");
        const repo: string = String(req.query["repo"] ?? "react");
        const page: number = Number(req.query["page"] ?? 1);
        const perPage: number = Number(req.query["perPage"] ?? 30);

        try {
            const {data: result} = await githubApi.get("/search/issues", {
                params: {
                    q: "repo:" + owner + "/" + repo + " type:issue state:open",
                    page,
                    per_page: perPage
                }
            });

            res.send({...result, result: "success"});
        } catch (error) {
            if (error instanceof AxiosError) {
                res.send({
                    result: "error",
                    status: error.response?.status ?? null,
                    message: error.message
                });
            } else {
                throw error;
            }
        }
    });

    router.get("/:number", loggerMiddleware({type: "get_issue"}), async (req, res) => {
        const owner: string = String(req.query["owner"] ?? "facebook");
        const repo: string = String(req.query["repo"] ?? "react");
        const number: number = Number(req.params["number"] ?? 1);

        try {
            const {data: result} = await githubApi.get(
                "/repos/" + owner + "/" + repo + "/issues/" + number
            );

            res.send({...result, result: "success"});
        } catch (error) {
            if (error instanceof AxiosError) {
                res.send({
                    result: "error",
                    status: error.response?.status ?? null,
                    message: error.message
                });
            } else {
                throw error;
            }
        }
    });

    return router;
}
