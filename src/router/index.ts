import {Router} from "express";
import {createIssuesRouter} from "./issues";
import {createLogsRouter} from "./logs";

export function createAppRouter(): Router {
    const router: Router = Router();

    router.use("/issues", createIssuesRouter());
    router.use("/logs", createLogsRouter());

    return router;
}
