import express, {Express} from "express";
import cors from "cors";
import "module-alias/register";
import {serverPort} from "@common/config";
import {connectDb} from "@common/db";
import {createAppRouter} from "./router";

(async () => {
    await connectDb();

    const app: Express = express();

    app.use(cors());
    app.use(createAppRouter());
    app.listen(serverPort, () => {
        console.log("Github Issue Tracker Backend listening on port " + serverPort);
    });
})();
